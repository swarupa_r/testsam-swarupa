﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using FizzBuzzApplication.Controllers;
using FizzBuzzRuleEngine;
using System.Web.Mvc;
using FizzBuzzApplication.Models;
using FizzBuzzApplication.CustomHTMLHelper;
using System.Web;

namespace FizzBuzzApplication.UnitTests
{
    public class LabelFizzBuzzHelperTest
    {
        [TestCase(Result = true)]
        public bool LabelWithCustomStyleTest()
        {
            IHtmlString htmlString;
            htmlString = FizzBuzzCustomHTMLHelper.LabelWithCustomStyle(null, "Test", "");
            return htmlString.GetType().Equals(typeof(HtmlString));
        }
    }
}
