﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using FizzBuzzApplication.Controllers;
using FizzBuzzRuleEngine;
using System.Web.Mvc;
using FizzBuzzApplication.Models;

namespace FizzBuzzApplication.UnitTests
{
    public class FizzBuzzControllerTest
    {
        private FizzBuzzController fizzbuzzController;
        private Mock<IRuleHandler> fizzbuzzRuleHandler;

        [TestFixtureSetUp]
        public void Setup()
        {
            fizzbuzzRuleHandler = new Mock<IRuleHandler>();
            fizzbuzzController = new FizzBuzzController(fizzbuzzRuleHandler.Object);
        }

        [TestCase(5, Result = true)]
        public bool FizzBuzzResultTest(int maxLimit)
        {
            FizzBuzzModel fizzbuzzModel = new FizzBuzzModel() { maxLimit = 10 };
            ViewResult returnedView = fizzbuzzController.FizzBuzzResult(fizzbuzzModel) as ViewResult;
            return returnedView.ViewName.Equals("FizzBuzzResult");
        }
    }
}
