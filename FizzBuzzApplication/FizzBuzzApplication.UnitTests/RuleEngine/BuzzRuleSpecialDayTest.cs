﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using FizzBuzzRuleEngine;
using FizzBuzzRuleEngine.Constants;

namespace FizzBuzzApplication.UnitTests
{
    /// <summary>
    /// BuzzRuleTest : This test class is to test the Buzz rules defined in rule engine
    /// </summary>
    public class BuzzRuleSpecialDayTest
    {
        private BuzzRule buzzRule;

        [TestFixtureSetUp]
        public void Setup()
        {
            Utility.SetCurrentDayNumber(DayOfWeek.Wednesday);
            buzzRule = new BuzzRule();
        }

        [TestCase(5, Result = BuzzConstants.SPECIALDISPLAYTEXT)]
        public string PossibleTextTest_Buzz_Wednesday(int inputNumber)
        {
            return buzzRule.PossibleText(inputNumber).displayText;
        }

        [TestCase(15, Result = BuzzConstants.SPECIALDISPLAYTEXT)]
        public string PossibleTextTest_FizzBuzz_Wednesday(int inputNumber)
        {
            return buzzRule.PossibleText(inputNumber).displayText;
        }

        [TestCase(6, Result = null)]
        public FizzBuzzEntity PossibleTextTest_NonBuzz_Wednesday(int inputNumber)
        {
            return buzzRule.PossibleText(inputNumber);
        }
    }
}
