﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using FizzBuzzRuleEngine;
using FizzBuzzRuleEngine.Constants;

namespace FizzBuzzApplication.UnitTests
{
    /// <summary>
    /// BuzzRuleTest : This test class is to test the Buzz rules defined in rule engine
    /// </summary>
    public class BuzzRuleTest
    {
        private BuzzRule buzzRule;
        [TestFixtureSetUp]
        public void Setup()
        {
            Utility.SetCurrentDayNumber(DayOfWeek.Thursday);
            buzzRule = new BuzzRule();
        }

        [TestCase(5, Result = BuzzConstants.DISPLAYTEXT)]
        public string PossibleTextTest_Buzz(int inputNumber)
        {
            return buzzRule.PossibleText(inputNumber).displayText;
        }

        [TestCase(15, Result = BuzzConstants.DISPLAYTEXT)]
        public string PossibleTextTest_FizzBuzz(int inputNumber)
        {
            return buzzRule.PossibleText(inputNumber).displayText;
        }

        [TestCase(3, Result = null)]
        public FizzBuzzEntity PossibleTextTest_NonBuzz(int inputNumber)
        {
             return buzzRule.PossibleText(inputNumber);
        }
    }
}
