﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using FizzBuzzRuleEngine;
using FizzBuzzRuleEngine.Constants;

namespace FizzBuzzApplication.UnitTests
{
    /// <summary>
    /// FizzRuleTest : This test class is to test the Fuzz rules defined in rule engine
    /// </summary>
    public class FizzRuleSpecialDayTest
    {
        private FizzRule fizzRule;
        [TestFixtureSetUp]
        public void Setup()
        {
            Utility.SetCurrentDayNumber(DayOfWeek.Wednesday);
            fizzRule = new FizzRule();
        }

        [TestCase(3, Result = FizzConstants.SPECIALDISPLAYTEXT)]
        public string PossibleTextTest_Fizz_Wednesday(int inputNumber)
        {
            return fizzRule.PossibleText(inputNumber).displayText;

        }
        
        [TestCase(15, Result = FizzConstants.SPECIALDISPLAYTEXT)]
        public string PossibleTextTest_FizzBuzz_Wednesday(int inputNumber)
        {
            return fizzRule.PossibleText(inputNumber).displayText;
        }

        [TestCase(5, Result = null)]
        public FizzBuzzEntity PossibleTextTest_NonFizz_Wednesday(int inputNumber)
        {
            return fizzRule.PossibleText(inputNumber);
        }
    }
}
