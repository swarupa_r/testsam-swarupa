﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using FizzBuzzRuleEngine;
using FizzBuzzRuleEngine.Constants;

namespace FizzBuzzApplication.UnitTests
{
    /// <summary>
    /// FizzRuleTest : This test class is to test the Fuzz rules defined in rule engine
    /// </summary>
    public class FizzRuleTest
    {
        private FizzRule fizzRule;
        [TestFixtureSetUp]
        public void Setup()
        {
            Utility.SetCurrentDayNumber(DayOfWeek.Tuesday);
            fizzRule = new FizzRule();
        }

        [TestCase(3, Result = FizzConstants.DISPLAYTEXT)]
        public string PossibleTextTest_Fizz(int inputNumber)
        {
            return fizzRule.PossibleText(inputNumber).displayText;

        }

        [TestCase(15, Result = FizzConstants.DISPLAYTEXT)]
        public string PossibleTextTest_FizzBuzz(int inputNumber)
        {
            return fizzRule.PossibleText(inputNumber).displayText;
        }

        [TestCase(5, Result = null)]
        public FizzBuzzEntity PossibleTextTest_NonFizz(int inputNumber)
        {
            return fizzRule.PossibleText(inputNumber);
        }
    }
}
