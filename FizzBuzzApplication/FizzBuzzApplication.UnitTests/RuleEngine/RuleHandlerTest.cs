﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using FizzBuzzRuleEngine;
namespace FizzBuzzApplication.UnitTests
{
    /// <summary>
    /// RuleHandlerTest : This test class is to test the RuleHandler mechanism, on how the execution of Rule Engine works.
    /// </summary>
    public class RuleHandlerTest
    {
        private RuleHandler fizzBuzzRuleHandler;
        private Mock<IRule> fizzRule;
        private Mock<IRule> buzzRule;

        [TestFixtureSetUp]
        public void Setup()
        {
            fizzRule = new Mock<IRule>();
            buzzRule = new Mock<IRule>();
            var rules = new List<IRule>();
            rules.Add(fizzRule.Object);
            rules.Add(buzzRule.Object);
            fizzBuzzRuleHandler = new RuleHandler(rules);
        }

        [TestCase(10, Result = 10)]
        public int ExecuteRulesTest(int maxLimit)
        {
            int resultCnt = 0;
            resultCnt = fizzBuzzRuleHandler.ExecuteRules(maxLimit).Count;
            return resultCnt;
        }
    }
}
