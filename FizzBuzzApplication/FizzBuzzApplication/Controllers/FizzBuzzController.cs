﻿using FizzBuzzApplication.Models;
using FizzBuzzRuleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzzApplication.Controllers
{
    public class FizzBuzzController : Controller
    {
        IRuleHandler ruleHandlr;
        public FizzBuzzController(IRuleHandler rhs)
        {
            ruleHandlr = rhs;
        }
        
        public ViewResult FizzBuzzResult(FizzBuzzModel fizzbuzzModel)
        {
            if (ModelState.IsValid)
            {
                fizzbuzzModel.listofNumber = ruleHandlr.ExecuteRules(fizzbuzzModel.maxLimit);

            }
            return View("FizzBuzzResult", fizzbuzzModel);
        }
    }
}