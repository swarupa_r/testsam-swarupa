﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using System.Web.Routing;
using FizzBuzzRuleEngine;

namespace FizzBuzzApplication.CustomControllerFactory
{
    public class FizzBuzzCustomControllerFactory : DefaultControllerFactory
    {
        /// <summary>
        /// GetControllerInstance : Creates the controller instances based on the type received.
        /// <remarks>
        /// Using Unity framework to register the dependencies, to inject the object thru constructor(Construction Injection).
        /// </remarks>
        /// </summary>
        /// <param name="requestContext"></param>
        /// <param name="controllerType"></param>
        /// <returns></returns>
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            IUnityContainer objContainer = new UnityContainer();
            objContainer.RegisterType<IRule, FizzRule>(typeof(FizzRule).Name);
            objContainer.RegisterType<IRule, BuzzRule>(typeof(BuzzRule).Name);
            objContainer.RegisterType<RuleHandler>(new InjectionConstructor(
              objContainer.ResolveAll<IRule>().ToList()));
            RuleHandler repp = objContainer.Resolve<RuleHandler>();
            IController controller = Activator.CreateInstance(controllerType, repp) as Controller;
            return controller;
        }
    }
}