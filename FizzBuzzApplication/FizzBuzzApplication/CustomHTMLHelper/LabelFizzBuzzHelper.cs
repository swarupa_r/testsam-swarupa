﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzzApplication.CustomHTMLHelper
{
    public static class FizzBuzzCustomHTMLHelper
    {
        public static IHtmlString LabelWithCustomStyle(this HtmlHelper helper, string content, string cssClass)
        {
            string htmlString = String.Format("<label class={1}>{0} </label>", content, cssClass);
            return new HtmlString(htmlString);
        }
    }
}