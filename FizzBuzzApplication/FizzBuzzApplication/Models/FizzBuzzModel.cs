﻿using FizzBuzzRuleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace FizzBuzzApplication.Models
{
    public class FizzBuzzModel
    {
        [Range(1, 1000)]
        [Display(Name = "Enter Upper Limit")]
        public int maxLimit { get; set; }
        public IList<FizzBuzzEntityList> listofNumber { get; set; }
    }
}