﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRuleEngine.Constants
{
   
    /// <summary>
    /// BuzzConstants : This class holds the Buzz constants, used by Rule Engine.
    /// </summary>
    public static class BuzzConstants
    {
        public const string RULEID = "BuzzRule";
        public const string DISPLAYTEXT = "Buzz";
        public const string SPECIALDISPLAYTEXT = "Wuzz";
        public const int BASENUMBER = 5;
    }
}
