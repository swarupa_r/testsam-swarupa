﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRuleEngine.Constants
{
    /// <summary>
    /// FizzConstants : This class holds the Fizz constants, used by Rule Engine.
    /// </summary>
    public static class FizzConstants
    {
        public const string RULEID = "FizzRule";
        public const string DISPLAYTEXT = "Fizz";
        public const string SPECIALDISPLAYTEXT = "Wizz";
        public const int BASENUMBER = 3;
    }
}
