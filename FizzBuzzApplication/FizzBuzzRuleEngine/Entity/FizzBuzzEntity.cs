﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRuleEngine
{

    /// <summary>
    /// FizzBuzzEntity : Holds the attributes or values, resulted from Fizz-Buzz rules, based on given input
    /// </summary>
    public class FizzBuzzEntity
    {
        public string displayText;
        public string appliedRuleId;
        public FizzBuzzEntity(string displayTxt, string appiedRlId)
        {
            displayText = displayTxt;
            appliedRuleId = appiedRlId;
        }
    }

}
