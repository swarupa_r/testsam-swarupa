﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRuleEngine
{
    /// <summary>
    /// FizzBuzzEntityList : List of FizzBuzzEntity, which holds the collection, if the input number satisfies both Fizz & Buzz Rule.
    /// </summary>
    public class FizzBuzzEntityList : List<FizzBuzzEntity>
    {

    }

}
