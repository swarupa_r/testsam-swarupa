﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRuleEngine
{
    /// <summary>
    /// IRuleHandler : Interface to support the execution of Fizz-Buzz Rules. It acts as abstraction.
    /// </summary>
    public interface IRuleHandler
    {
         IList<FizzBuzzEntityList> ExecuteRules(int maxLimit);
    }
}
