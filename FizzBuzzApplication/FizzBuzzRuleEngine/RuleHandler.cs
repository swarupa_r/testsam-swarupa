﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRuleEngine
{
    /// <summary>
    /// RuleHandler : Implements IRuleHandler which holds the logic of Rule exceution. Acts as a abstraction of Fizz-Buzz rules.
    /// </summary>
    public class RuleHandler : IRuleHandler
    {
        private IEnumerable<IRule> rulesObject;
        public RuleHandler(IEnumerable<IRule> rules)
        {
            rulesObject = rules;
        }
        public IList<FizzBuzzEntityList> ExecuteRules(int maxLimit)
        {
            FizzBuzzEntityList result;
            IList<FizzBuzzEntityList> resultList = new List<FizzBuzzEntityList>();
            try
            { 
                for (int number = 1; number <= maxLimit; number++)
                {
                    result = new FizzBuzzEntityList();
                    foreach (IRule ruleObj in rulesObject)
                    {
                        FizzBuzzEntity fizzBuzzEnty = ruleObj.PossibleText(number);
                        if (fizzBuzzEnty != null)
                            result.Add(fizzBuzzEnty);
                    }
                    if (result.Count == 0)
                    {
                        result.Add(new FizzBuzzEntity(number.ToString(), string.Empty));
                    }
                    resultList.Add(result);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return resultList;
        }

    }
}
