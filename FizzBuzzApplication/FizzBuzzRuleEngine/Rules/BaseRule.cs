﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRuleEngine
{
    /// <summary>
    /// BaseRule : Implements the IRule and holds the common logic Fizz-Buzz Rules
    /// </summary>
    public abstract class BaseRule : IRule
    {
        public string DisplayText { get; set; }
        public string RuleID { get; set; }
        public int BaseNumber { get; set; }

        public FizzBuzzEntity PossibleText(int inputNumber)
        {
            try
            {
                if (inputNumber % BaseNumber == 0)
                {
                    return new FizzBuzzEntity(DisplayText, RuleID);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return null;
        }

    }
}
