﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzzRuleEngine.Constants;

namespace FizzBuzzRuleEngine
{
    /// <summary>
    /// BuzzRule : Supplies the required constants or values to BaseRule class, required to execute the Buzz Rule.
    /// </summary>
    public class BuzzRule : BaseRule
    {
        public BuzzRule()
        {
            DisplayText = Utility.IsToday(DayOfWeek.Wednesday) ? BuzzConstants.SPECIALDISPLAYTEXT : BuzzConstants.DISPLAYTEXT;
            RuleID = BuzzConstants.RULEID;
            BaseNumber = BuzzConstants.BASENUMBER;
        }
    }
}
