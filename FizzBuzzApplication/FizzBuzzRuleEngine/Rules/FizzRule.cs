﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzzRuleEngine.Constants;

namespace FizzBuzzRuleEngine
{
    /// <summary>
    /// FizzRule : Supplies the required constants or values to BaseRule class, required to execute the Fizz Rule.
    /// </summary>
    public class FizzRule : BaseRule
    {
        public FizzRule()
        {
            DisplayText = Utility.IsToday(DayOfWeek.Wednesday) ? FizzConstants.SPECIALDISPLAYTEXT : FizzConstants.DISPLAYTEXT;
            RuleID = FizzConstants.RULEID;
            BaseNumber = FizzConstants.BASENUMBER;
        }
    }
}
