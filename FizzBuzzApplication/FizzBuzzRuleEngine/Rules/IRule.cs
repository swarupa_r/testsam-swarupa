﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzzRuleEngine;

namespace FizzBuzzRuleEngine
{
    /// <summary>
    /// IRule : Interface to support and define blue print of Fizz-Buzz Rules
    /// </summary>
    public interface IRule
    {
         FizzBuzzEntity PossibleText(int inputNumber);
    }
}
