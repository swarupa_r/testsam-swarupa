﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRuleEngine
{
    /// <summary>
    /// Utility class, responsible to cross-check if the current day set explicityly or implicitly, matches with the special day or not(Eg: Wedensday)
    /// </summary>
    public class Utility
    {
        static DayOfWeek currentDayNumber = DateTime.Now.DayOfWeek;
        public static bool IsToday(DayOfWeek dayNumber)
        {
            return currentDayNumber.Equals(dayNumber);
        }
        public static void SetCurrentDayNumber(DayOfWeek currDayNumb)
        {
            currentDayNumber = currDayNumb;
        }

    }
}
